<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
  "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % languagedata SYSTEM "language.ent" > %languagedata;
  <!ENTITY % shareddata   SYSTEM "../release-notes.ent" > %shareddata;
]>

<chapter id="ch-whats-new" lang="en">
<title>What's new in &debian; &release;</title>
<para>
The <ulink url="&url-wiki-newinstretch;">Wiki</ulink> has more information
about this topic.
</para>

<!-- 
Sources for architecture status:
  https://release.debian.org/stretch/arch_qualify.html

Some descriptions of the ports: https://www.debian.org/ports/
-->
<section>
<title>Supported architectures</title>

<para>
  Debian &release; introduces one new architecture:
</para>
<itemizedlist>
  <listitem>
    <para>
      64-bit little-endian MIPS (<literal>mips64el</literal>)
    </para>
  </listitem>
</itemizedlist>

<para>
  Debian &release; regrettably removes support for the following architecture:
</para>
<itemizedlist>
  <listitem>
    <para>
      PowerPC (<literal>powerpc</literal>)
    </para>
  </listitem>
</itemizedlist>

<variablelist arch="i386">
  <varlistentry arch="i386">
    <term>Support for 32-bit PCs no longer covers vanilla i586</term>
    <!-- Jessie to Stretch -->
    <listitem>
      <para>
        The 32-bit PC support (known as the Debian architecture
        <literal>i386</literal>) now no longer covers a plain i586
        processor.  The new baseline is the i686, although some i586
        processors (e.g. the <quote>AMD Geode</quote>) will remain supported.
      </para>
      <para>
        Please refer to <xref linkend="i386-is-now-almost-i686"/> for
        more information.
      </para>
    </listitem>
  </varlistentry>
  <!--
    the block below is needed for successful build;
    the upper only exist in i386 version so all others stop w/ the error:
      stdin.tex:226: Something's wrong-\-perhaps a missing \item.
      stdin.tex:226: leading text: \end{description}
    so, keep this until common item comes.
  -->
  <!--varlistentry arch="not-i386">
    <term>FIXME</term>
    <listitem>
      <para>
        FIXME: add item here
      </para>
    </listitem>
  </varlistentry-->
</variablelist>

<para>
The following are the officially supported architectures for &debian;
&release;:
</para>
<itemizedlist>
<listitem>
<para>
32-bit PC (<literal>i386</literal>) and 64-bit PC (<literal>amd64</literal>)
</para>
</listitem>
<listitem>
<para>
64-bit ARM (<literal>arm64</literal>)
</para>
</listitem>
<listitem>
<para>
ARM EABI (<literal>armel</literal>)
</para>
</listitem>
<listitem>
<para>
ARMv7 (EABI hard-float ABI, <literal>armhf</literal>)
</para>
</listitem>
<listitem>
<para>
MIPS (<literal>mips</literal> (big-endian) and <literal>mipsel</literal> (little-endian))
</para>
</listitem>
<listitem>
<para>
64-bit little-endian MIPS (<literal>mips64el</literal>)
</para>
</listitem>
<listitem>
<para>
64-bit little-endian PowerPC (<literal>ppc64el</literal>)
</para>
</listitem>
<listitem>
<para>
IBM System z (<literal>s390x</literal>)
</para>
</listitem>
</itemizedlist>

<para>
You can read more about port status, and port-specific information for your
architecture at the <ulink url="&url-ports;">Debian port
web pages</ulink>.
</para>

</section>

<section id="newdistro">
<title>What's new in the distribution?</title>

<programlisting condition="fixme">
 TODO: Make sure you update the numbers in the .ent file 
     using the changes-release.pl script found under ../
</programlisting>

<para>
This new release of Debian again comes with a lot more software than
its predecessor &oldreleasename;; the distribution includes over
&packages-new; new packages, for a total of over &packages-total;
packages.  Most of the software in the distribution has been updated:
over &packages-updated; software packages (this is
&packages-update-percent;% of all packages in &oldreleasename;).
Also, a significant number of packages (over &packages-removed;,
&packages-removed-percent;% of the packages in &oldreleasename;) have
for various reasons been removed from the distribution.  You will not
see any updates for these packages and they will be marked as
"obsolete" in package management front-ends; see <xref
linkend="obsolete"/>.
</para>

<para>
  &debian; again ships with several desktop applications and
  environments.  Among others it now includes the desktop environments
  GNOME<indexterm><primary>GNOME</primary></indexterm> 3.22,
  KDE Plasma<indexterm><primary>KDE</primary></indexterm> 5.8,
  LXDE<indexterm><primary>LXDE</primary></indexterm>,
  LXQt<indexterm><primary>LXQt</primary></indexterm> 0.11,
  MATE<indexterm><primary>MATE</primary></indexterm> 1.16, and
  Xfce<indexterm><primary>Xfce</primary></indexterm> 4.12.
</para>
<para>
  Productivity applications have also been upgraded, including the
  office suites: 
</para>
  <itemizedlist>
  <listitem>
    <para>
      LibreOffice<indexterm><primary>LibreOffice</primary></indexterm>
      is upgraded to version 5.2;
    </para>
  </listitem>
  <listitem>
    <para>
      Calligra<indexterm><primary>Calligra</primary></indexterm>
      is upgraded to 2.9.
    </para>
  </listitem>
  <!-- no updates?
  <listitem>
    <para>
      GNUcash<indexterm><primary>GNUcash</primary></indexterm> is upgraded to 2.6;
    </para>
  </listitem>
  <listitem>
    <para>
      GNUmeric<indexterm><primary>GNUmeric</primary></indexterm> is upgraded to 1.12;
    </para>
  </listitem>
  <listitem>
    <para>
      Abiword<indexterm><primary>Abiword</primary></indexterm> is upgraded to 3.0.
      </listitem>
      </ -->
  </itemizedlist>
<para>
  Updates of other desktop applications include the upgrade to
  Evolution<indexterm><primary>Evolution</primary></indexterm> 3.22.
</para>

<!-- JFS: 
Might it be useful point to http://distrowatch.com/table.php?distribution=debian ? 
This provides a more comprehensive comparison among different releases -->

<para>
Among many others, this release also includes the following software updates:
</para>
<informaltable pgwide="1">
  <tgroup cols="3">
    <colspec align="justify"/>
    <colspec align="justify"/>
    <colspec align="justify"/>
    <!-- colspec align="justify" colwidth="3*"/ -->
    <thead>
      <row>
	<entry>Package</entry>
	<entry>Version in &oldrelease; (&oldreleasename;)</entry>
	<entry>Version in &release; (&releasename;)</entry>
      </row>
    </thead>
    <tbody>
      <!-- 
      <row id="new-apache2">
	<entry>Apache<indexterm><primary>Apache</primary></indexterm></entry>
	<entry>2.4.10</entry>
	<entry>2.4.23</entry>
      </row>
        /-->
      <row id="new-bind9">
	<entry>BIND<indexterm><primary>BIND</primary></indexterm> <acronym>DNS</acronym> Server</entry>
	<entry>9.9</entry>
	<entry>9.10</entry>
      </row>
<!--
      <row id="new-chromium">
	<entry>Chromium<indexterm><primary>Chromium</primary></indexterm></entry>
	<entry>53.0</entry>
	<entry>53.0</entry>
      </row>
      <row id="new-courier">
	<entry>Courier<indexterm><primary>Courier</primary></indexterm> <acronym>MTA</acronym></entry>
	<entry>0.73</entry>
	<entry>0.76</entry>
      </row>
      <row id="new-dia">
	<entry>Dia<indexterm><primary>Dia</primary></indexterm></entry>
	<entry>0.97.2</entry>
	<entry>0.97.3</entry>
        </row>
      <row id="new-dovecot">
	<entry>Dovecot<indexterm><primary>Dovecot</primary></indexterm> <acronym>MTA</acronym></entry>
	<entry>2.2</entry>
	<entry>2.2</entry>
      </row>
-->
      <row id="new-emacs">
	<entry>Emacs</entry>
	<entry>24.4</entry>
	<entry>24.5 and 25.1</entry>
      </row>
      <row id="new-exim4">
	<entry>Exim<indexterm><primary>Exim</primary></indexterm> default e-mail server</entry>
	<entry>4.84</entry>
	<entry>4.88</entry>
      </row>
<!--
      <row id="new-firefox">
	<entry>Firefox<indexterm><primary>Firefox</primary></indexterm></entry>
	<entry>45.5 (AKA Iceweasel)</entry>
	<entry>50.0</entry>
      </row>
-->
      <row id="new-gcc">
	<entry><acronym>GNU</acronym> Compiler Collection as default compiler<indexterm><primary>GCC</primary></indexterm></entry>
	<entry>4.9</entry>
	<entry>6.3</entry>
      </row>
<!--
      <row id="new-gimp">
	<entry><acronym>GIMP</acronym><indexterm><primary>GIMP</primary></indexterm></entry>
	<entry>2.8</entry>
	<entry>2.8</entry>
      </row>
-->
      <row id="new-gnupg">
	<entry>GnuPG<indexterm><primary>GnuPG</primary></indexterm></entry>
	<entry>1.4</entry>
	<entry>2.1</entry>
      </row>
      <row id="new-inkscape">
	<entry>Inkscape<indexterm><primary>Inkscape</primary></indexterm></entry>
	<entry>0.48</entry>
	<entry>0.91</entry>
      </row>
      <row id="new-libc6">
	<entry>the <acronym>GNU</acronym> C library</entry>
	<entry>2.19</entry>
	<entry>2.24</entry>
      </row>
<!--
      <row id="new-lighttpd">
	<entry>lighttpd</entry>
	<entry>1.4.35</entry>
	<entry>1.4.39</entry>
      </row>
-->  
      <row id="new-linux-image">
        <entry>Linux kernel image</entry>
        <entry>3.16 series</entry>
        <entry>4.9 series</entry>
      </row>

    <row id="new-mariadb">
	<entry>MariaDB<indexterm><primary>MariaDB</primary></indexterm></entry>
	<entry>10.0</entry>
	<entry>10.1</entry>
      </row>

      <row id="new-nginx">
	<entry>Nginx<indexterm><primary>Nginx</primary></indexterm></entry>
	<entry>1.6</entry>
	<entry>1.10</entry>
      </row>
<!--
      <row id="new-openldap">
	<entry>OpenLDAP</entry>
	<entry>2.4.40</entry>
	<entry>2.4.44</entry>
      </row>
-->
      <row id="new-openjdk">
	<entry>OpenJDK<indexterm><primary>OpenJDK</primary></indexterm></entry>
	<entry>7</entry>
	<entry>8</entry>
      </row>
      <row id="new-openssh">
	<entry>OpenSSH<indexterm><primary>OpenSSH</primary></indexterm></entry>
	<entry>6.7p1</entry>
	<entry>7.4p1</entry>
      </row>
      <row id="new-perl">
	<entry>Perl<indexterm><primary>Perl</primary></indexterm></entry>
	<entry>5.20</entry>
	<entry>5.24</entry>
      </row>
      <row id="new-php">
	<entry><acronym>PHP</acronym><indexterm><primary>PHP</primary></indexterm></entry>
	<entry>5.6</entry>
	<entry>7.0</entry>
      </row>
      <row id="new-postfix">
	<entry>Postfix<indexterm><primary>Postfix</primary></indexterm> <acronym>MTA</acronym></entry>
	<entry>2.11</entry>
	<entry>3.1</entry>
      </row>
      <row id="new-postgresql">
	<entry>PostgreSQL<indexterm><primary>PostgreSQL</primary></indexterm></entry>
	<entry>9.4</entry>
	<entry>9.6</entry>
      </row>
<!--
      <row id="new-python">
	<entry>Python</entry>
	<entry>2.6</entry>
	<entry>2.7</entry>
      </row>
-->
      <row id="new-python3">
	<entry>Python 3</entry>
	<entry>3.4</entry>
	<entry>3.5</entry>
      </row>
      <row id="new-samba">
	<entry>Samba</entry>
	<entry>4.1</entry>
	<entry>4.5</entry>
      </row>
      <row id="new-vim">
	<entry>Vim</entry>
	<entry>7</entry>
	<entry>8</entry>
      </row>
    </tbody>
  </tgroup>
</informaltable>

<programlisting condition="fixme">
 TODO: (JFS) List other server software? RADIUS? Streaming ?
</programlisting>

<section id="cd">
<title>CDs, DVDs, and BDs</title>
<para>
The official &debian; distribution now ships on 12 to 14 binary
<acronym>DVD</acronym>s <indexterm><primary>DVD</primary></indexterm>
(depending on the architecture) and 12 source
<acronym>DVD</acronym>s. Additionally, there is a
<emphasis>multi-arch</emphasis> <acronym>DVD</acronym>, with a subset
of the release for the <literal>amd64</literal> and
<literal>i386</literal> architectures, along with the source
code. &debian; is also released as Blu-ray
<indexterm><primary>Blu-ray</primary></indexterm>
(<acronym>BD</acronym>) and dual layer Blu-ray
<indexterm><primary>dual layer Blu-ray</primary></indexterm>
(<acronym>DLBD</acronym>) images for the <literal>amd64</literal> and
<literal>i386</literal> architectures, and also for source
code. &debian; used to be released as a very large set of
<acronym>CD</acronym>s for each architecture, but with the &releasename;
release these have been dropped.
</para>
</section>

<section id="security">
  <title>Security</title>

  <para>
    For the stretch release, the Debian version of the GNU GCC 6
    compiler now defaults to compiling "position independent
    executables" (PIE).  Accordingly the vast majority of all
    executables will now support <ulink
    url="https://en.wikipedia.org/wiki/Address_space_layout_randomization">address
    space layout randomization (ASLR)</ulink>, which is a mitigation
    for a number of exploits that are now probabilistic rather than
    deterministic.
  </para>

</section>

<section id="gcc-versions">
  <title>GCC versions</title>

  <para>
    Debian stretch includes only version 6 of the GNU GCC compiler,
    which may impact users expecting version 4.x or 5.x to be available.
    See the <ulink url="https://wiki.debian.org/GCC5">GCC5</ulink> and
    <ulink url="https://wiki.debian.org/GCC6">GCC6</ulink> wiki pages
    for more information about the transition.
  </para>

</section>

<section id="mariadb-replaces-mysql">
  <title>MariaDB replaces MySQL</title>
  <para>
    MariaDB is now the default MySQL variant in Debian, at version 10.1.
    The &releasename; release introduces a new mechanism for switching the
    default variant, using metapackages created from the
    <systemitem role="package">mysql-defaults</systemitem> source package.
    For example, installing the metapackage
    <systemitem role="package">default-mysql-server</systemitem>
    will install
    <systemitem role="package">mariadb-server-10.1</systemitem>.
    Users who had
    <systemitem role="package">mysql-server-5.5</systemitem> or
    <systemitem role="package">mysql-server-5.6</systemitem> will have it
    removed and replaced by the MariaDB equivalent.
    Similarly, installing
    <systemitem role="package">default-mysql-client</systemitem>
    will install
    <systemitem role="package">mariadb-client-10.1</systemitem>.
    </para>
    <important><para>
    Note that the database binary data file formats are not backwards
    compatible, so once you have upgraded to MariaDB 10.1 you will
    not be able to switch back to any previous version of MariaDB or
    MySQL unless you have a proper database dump. Therefore, before
    upgrading, please make backups of all important databases with
    an appropriate tool such as <command>mysqldump</command>.
    </para></important>
    <para>
    The <systemitem role="package">virtual-mysql-*</systemitem> and
    <systemitem role="package">default-mysql-*</systemitem> packages
    will continue to exist. MySQL continues to be maintained in Debian,
    in the <literal>unstable</literal> release.
    See the <ulink url="https://wiki.debian.org/Teams/MySQL">Debian MySQL Team
    wiki page</ulink> for current information about the mysql-related
    software available in Debian.
  </para>
</section>

<section id="apt-improvements">
  <title>Improvements to APT and archive layouts</title>
  <para>
    The <systemitem role="package">apt</systemitem> package manager
    has seen a number of improvements since jessie.  Most of these
    apply to <systemitem role="package">aptitude</systemitem> as well.
    Following are selected highlights of some of these.
  </para>
  <para>
    On the security side, APT now rejects weaker checksums by default
    (e.g. SHA1) and attempts to download as an unprivileged user.
    Please refer to <xref linkend="apt-new-requirements-to-mirrors" />
    and <xref linkend="apt-unpriv-acquire" /> for more information.
  </para>
  <para>
    The APT-based package managers have also gotten a number of
    improvements that will remove the annoying <quote>hash sum mismatch</quote>
    warning that occurs when running apt during a mirror
    synchronization.  This happens via the new
    <literal>by-hash</literal> layout, which enables APT to download
    metadata files by their content hash.
  </para>
  <para>
    If you use third-party repositories, you may still experience
    these intermittent issues, if the vendor does not provide the
    <literal>by-hash</literal> layout.  Please recommend them to adopt
    this layout change.  A very short technical description is
    available in the <ulink
    url="https://wiki.debian.org/DebianRepository/Format">Repository
    format description</ulink>.
  </para>
  <para>
    While this may be mostly interesting for mirror administrators, APT
    in stretch can use DNS (SRV) records to locate an HTTP backend.
    This is useful for providing a simple DNS name and then managing
    backends via DNS rather than using a <quote>redirector</quote> service.  This
    feature is also used by the new Debian mirror described in <xref
    linkend="deb-debian-org-mirror" />.
  </para>
</section>

<section id="deb-debian-org-mirror">
  <title>New deb.debian.org mirror</title>
  <para>
    Debian now provides a new additional service called <ulink
    url="https://deb.debian.org">deb.debian.org</ulink>.  It provides
    the content of the main archive, the security archive, ports and
    even our new debug archive (see <xref linkend="debug-archive" />)
    under a single easy to remember hostname.
  </para>
  <para>
    This service relies on the new DNS support in APT, but will
    fall back to a regular redirect for HTTPS access or older versions
    of APT.  More details are provided on <ulink
    url="https://deb.debian.org">deb.debian.org</ulink>.
  </para>
  <para>
    Thanks to Fastly and Amazon CloudFront for sponsoring the CDN backends
    behind this service.
  </para>
</section>

<section id="modern-gnupg">
  <title>Move to "Modern" GnuPG</title>
  <para>
    The stretch release is the first version of Debian to feature the
    <quote>modern</quote> branch of GnuPG in the <systemitem
    role="package">gnupg</systemitem> package.  This brings with it
    elliptic curve cryptography, better defaults, a more modular
    architecture, and improved smartcard support.  The modern branch
    also explicitly does not support some older, known-broken formats
    (like PGPv3).  See
    <filename>/usr/share/doc/gnupg/README.Debian</filename> for more
    information.
  </para>
  <para>
    We will continue to supply the <quote>classic</quote> branch of GnuPG as
    <systemitem role="package">gnupg1</systemitem> for people who need
    it, but it is now deprecated.
  </para>
</section>

<section id="debug-archive">
  <!-- jessie to stretch -->
  <title>A new archive for debug symbols</title>
  <note>
    <para>
      This section is mostly interesting for developers or if you wish
      to attach a full stack trace to a crash report.
    </para>
  </note>
  <para>
    Previously, the main Debian archive would include packages
    containing debug symbols for selected libraries or programs.  With
    stretch, most of these have been moved to a separate archive
    called the <literal>debian-debug</literal> archive.  This archive contains the
    debug symbol packages for the vast majority of all packages
    provided by Debian.
  </para>
  <para>
    If you want to fetch such debug packages, please include the following
    in your APT sources: <screen>
deb http://debug.mirrors.debian.org/debian-debug/ stretch-debug main
</screen>
    Alternatively, you can also fetch them from <ulink
    url="http://snapshot.debian.org">snapshot.debian.org</ulink>.
  </para>
  <para>
    Once enabled, you can now fetch debug symbols for the package in
    question by installing <systemitem
    role="package"><replaceable>pkg</replaceable>-dbgsym</systemitem>.
    Please note that individual packages may still provide a
    <systemitem
    role="package"><replaceable>pkg</replaceable>-dbg</systemitem>
    package in the main archive instead of the new dbgsym.
  </para>
</section>

<section id="new-interface-names">
  <!--Jessie to Stretch-->
  <title>New method for naming network interfaces</title>
  <para>
    The installer and newly installed systems will use a new standard
    naming scheme for network interfaces instead of <literal>eth0</literal>,
    <literal>eth1</literal>, etc.
    The old naming method suffered from enumeration race conditions
    that made it possible for interface names to change unexpectedly
    and is incompatible with mounting the root filesystem read-only.
    The new enumeration method relies on more sources of information,
    to produce a more repeatable outcome. It uses the firmware/BIOS
    provided index numbers and then tries PCI card slot numbers,
    producing names like <literal>ens0</literal> or <literal>enp1s1</literal>
    (ethernet) or <literal>wlp3s0</literal> (wlan).
    USB devices, which can be added to the system at any time, 
    will have names based upon their ethernet MAC addresses.
  </para>
  <para>
    This change does not apply to upgrades of jessie systems;
    the naming will continue to be enforced by
    <filename>/etc/udev/rules.d/70-persistent-net.rules</filename>.
    For more information, see 
    <filename>/usr/share/doc/udev/README.Debian.gz</filename>
    or the <ulink url="https://www.freedesktop.org/wiki/Software/systemd/PredictableNetworkInterfaceNames/">
    upstream documentation</ulink>.
  </para>
</section>

<section id="debian-med">
<title>News from Debian Med Blend</title>

<para>
    Besides several new packages and updates for software targeting
    life sciences and medicine, the Debian Med team has again put a focus on 
    the quality of the provided packages.  In a GSoC project and an 
    Outreachy project, two students worked hard to add Continuous Integration 
    support to the packages with the highest popularity-contest usage
    statistics.  The latest Debian Med sprint in Bucharest also
    concentrated on package testing.
</para>
<para>
    To install packages maintained by the Debian Med team, install the
    metapackages named med-*, which are at version 3.0.1 for Debian stretch.
    Feel free to visit the
    <ulink url="http://blends.debian.org/med/tasks">Debian Med tasks pages</ulink>
    to see the full range of biological and medical software available in Debian.
</para>
</section>

<section id="x-no-longer-requires-root">
  <title>The Xorg server no longer requires root</title>
  <para>
    In the stretch version of Xorg, it is possible to run the Xorg
    server as a regular user rather than as root.  This reduces the
    risk of privilege escalation via bugs in the X server.  However,
    it has some requirements for working:
  </para>
    <itemizedlist>
      <listitem>
        <para>
          It needs <command>logind</command> and <systemitem
          role="package">libpam-systemd</systemitem>.
        </para>
      </listitem>
      <listitem>
        <para>
          The system needs to support Kernel Mode Setting
          (<abbrev>KMS</abbrev>).  Therefore, it may not work in
          some virtualization environments (e.g. virtualbox) or if the kernel has no
          driver that supports your graphics card.
        </para>
      </listitem>
      <listitem>
        <para>
          It needs to run on the virtual console it was started from.
        </para>
      </listitem>
      <listitem>
        <para>
          Only the <systemitem role="package">gdm3</systemitem>
          display manager supports running X as a non-privileged user in
          stretch.  Other display managers will always run X as root.
          Alternatively, you can also start X manually as a non-root
          user on a virtual terminal via <command>startx</command>.
        </para>
      </listitem>
    </itemizedlist>
    <para>
      When run as a regular user, the Xorg log will be available from
      <filename>~/.local/share/xorg/</filename>.
    </para>
</section>

</section>
</chapter>
